// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

function getRandomInt(max) {
  min = 0;
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

$(document).ready(function() {
  $.getJSON("/quotes/quotes.json", function(json) {
    var quotes = json.quotes;

    if (document.title == 'Quotes I like | Andrew Zah') {
        quotes = quotes.sort(function(a,b) { a.author.localeCompare(b.author) });
        quotes.forEach(function(quote) {
          var quoteText = $("<p class='quote-text'></p>").text(quote.quote);
          var quoteSource = quote.source;
          var quoteAuthor = "";

          if (quote.author){
            quoteAuthor = "— " + quote.author;
          }
          if (quote.source) {
            quoteAuthor = quoteAuthor + ", "
          }

          $('#quotes').append(
            $("<div class='quote'/>").append(
              quoteText,
              $("<div/>").append(
                $("<span class='quote-author'/>").text(quoteAuthor),
                $("<span class='quote-source'/>").append(
                  $("<i/>").text(quoteSource)
                )
              )
            )
          );
        });
    } else {
      var length = quotes.length;
      var quote = quotes[getRandomInt(length)];

      $('#random-quote-quote').text(quote.quote);

      var author = "— " + quote.author
      if (quote.source) {
        $('#random-quote-author').text(author + ", ");
        $('#random-quote-source').text(quote.source);
      } else {
        $('#random-quote-author').text(author);
      }
    }
  })
  .fail(function() {
    console.log("Error loading json file–is there a json syntax issue?");
  });
});

