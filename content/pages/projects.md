+++
title = "Projects"
path = "projects"
template = "projects.html"
date = 2019-03-25
+++

These are various projects I've worked on over the years. I hope they may be of some use to you. If you'd like to contribute to any, feel free! I generally use the GPLv3 license, or MIT otherwise.

As of late the bulk of my development has been on private repos for work, unfortunately.

## Git Repo Stores
In preferred order. I use github when I have no other option and for mirrors.
* [git.sr.ht/~andrewzah](https://git.sr.ht/~andrewzah)
* [git.andrewzah.com/andrei](https://git.andrewzah.com/andrei)
* [gitlab.com/andrewzah](https://gitlab.com/andrewzah)
* [github.com/azah](https://github.com/azah)

---

## In Progress
These are things I'm actively working on and/or messing around with.

#### [apps.andrei.kr](https://git.sr.ht/~andrewzah/apps.andrei.kr) – rust/wasm/react
* Frontend for `korean_numbers` and other learning tools.
#### [unifiedsearch](https://git.sr.ht/~andrewzah/unifiedsearch) – rust/wasm/mithril
* This website. Statically generated using zola and tera templating.
#### [danmuji](https://git.sr.ht/~andrewzah/danmuji) – golang
* Discord bot written using Golang.
#### [naveraudio](https://git.sr.ht/~andrewzah/naver_audio) – rust
* Downloads sound files for Korean words via NAVER's (undocumented) api.

---

## Actively Maintained
#### [korean_numbers](https://git.sr.ht/~andrewzah/korean_numbers) – rust
* Converts integers and floats to Hangeul. Supports both number systems.
#### [personal_site](https://git.sr.ht/~andrewzah/personal-site) – zola
* This website. Statically generated using zola and tera templating.
#### [personal_docker](https://git.sr.ht/~andrewzah/andrewzah.com) – shell/docker
* Docker setup for fast deployments on VPSs.

---

## Inactive
### Crystal
#### [amaranth-cr](https://github.com/azah/amaranth)
* Discord bot based on [discordcr](https://github.com/meewo/discordcr).
* Wrote a lexer/parser to allow custom, recursive commands.
* Wrote a command and plugin framework to allow custom functionality.

#### [dotacr](https://git.andrewzah.com/andrei/dotacr)
* Wrote a wrapper for Valve's [DotA 2 api](https://dev.dota2.com/showthread.php?t=58317).

#### [cOWAPI](https://github.com/azah/cowapi)
* Wrote a Crystal/Kemal version of [OWAPI](https://github.com/SunDwarf/OWAPI), that scrapes and parses Overwatch gameplay stats from Blizzard.

#### [Minifluxcr](https://github.com/azah/miniflux-cr)
* Wrote a wrapper for Miniflux's api.

#### [tree-problem](https://github.com/azah/tree-problem)
* Solved an interview problem for CancerIQ. They wasted my time, so I made my solution public.

### Elm
#### [amaranth-frontend](https://github.com/azah/amaranth-frontend)
* Elm frontend for amaranth-bot that dynamically lists commands and info.

### Python 3
#### [OWAPI](https://github.com/Fuyukai/OWAPI)
* Contributed to v2 and v3 of Laura's overwatch stats api.
* Learned about xml scraping and parsing, and API design.

## Not Programming Related
#### [dotfiles](https://git.andrewzah.com/andrei/dotfiles)
* a dotfile a day keeps productivity at bay.
#### [az-quotes](https://git.andrewzah.com/andrei/quotes)
* A json file of quotes that I like, as seen on my [quotes page](/quotes). Source for the random quote at the end of my posts.
#### [tf2config](https://git.andrewzah.com/andrei/tf2-config)
* tf2 config based on master comfig.
#### [korean_articles](https://git.andrewzah.com/andrei/korean-articles) – pandoc
* Created a makefile to easily copy/paste Korean articles and render them as PDFs for printing.
