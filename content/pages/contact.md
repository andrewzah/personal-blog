+++
title = "Contact"
template = "plain.html"
path = "contact"
+++

Ways to contact me, in order of preference.

* email (strongly preferred): [zah@andrewzah.com](mailto:zah@andrewzah.com)

I do my best to respond to incoming email, during standard working hours typically. KST.

* discord: andrei#7237
* fediverse (Mastodon/Pleroma): [@andrewzah@niu.moe](https://niu.moe/@andrewzah)
