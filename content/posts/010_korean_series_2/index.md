+++
title = "Korean Progress #2"
slug = "korean-progress-2"
date = 2019-03-03

[taxonomies]
tags = ["korean", "learning"]
categories = ["korean-progress"]

[extra]
keywords = "korean hangeul language learning methods self study progress"
summary = "My Korean learning progress #2"
citations = [
  ["When(ever) I do V: ~ㄹ/을 때마다", "http://www.mykoreanramblings.com/2014/03/22/when-i-v-%EC%9D%84-%EB%95%8C/"],
  ["How to say 'feeling' in Korean (기분 vs. 느낌)", "https://www.youtube.com/watch?v=eHDYvrXekqM"],
  ["어쩌다", "https://www.howtostudykorean.com/upper-intermediate-korean-grammar/unit-4-lessons-92-100/lesson-94"],
  ["어쩌면", "https://www.howtostudykorean.com/unit-3-intermediate-korean-grammar/unit-3-lessons-59-66/lesson-59/"],
  ["아무리", "https://www.howtostudykorean.com/unit-2-lower-intermediate-korean-grammar/unit-2-lessons-42-50/lesson-48/"],
  ["어쩔 수 없다", "https://www.howtostudykorean.com/unit-5/lessons-109-116/lesson-110/"]
]
+++

While studying Korean, sometimes I get a bit lazy or lose some motivation. I decided to start this weekly series as a way to track what I've been learning and to motivate myself to keep studying.

<!-- more -->

Welcome to week #2!

## Things I'm struggling with

This week's biggest hurdle was motivation, I think. I ended up skipping Anki several times.

## This week's learning methods
* Going to a language exchange where I get 1-on-1 for an hour. Right now I go about 2-3 times per week.
* Texing friends in Korean
* Studying grammar points daily or every other day
* Using [Anki](https://apps.ankiweb.net/) to memorize and retain words.

As I mentioned last week, Anki has become somewhat of a slog. When I miss one day I hate coming back again because the review session will take twice as long (since I'll forget most of the freshly learned words). I'm not sure what the solution here is other than limiting words to review, but that could still cause a similar issue with forgetting words.

However, I did learn that one can bulk input to Anki via a CSV file, which is pretty neat.

## New Learning Techniques
This week I started reading [뚜벅뚜벅](https://comic.naver.com/bestChallenge/detail.nhn?titleId=709889&no=1), a webtoon about a woman who has no desire to get married. I desperately needed to change things up because my studying was growing somewhat stale. My current strategy is to read a chapter or two and try to look up as few words as possible while still understanding the sentences. Then I plan to go back and thoroughly analyze the grammar and words, and then finally re-read the chapter without looking anything up.

Webtoons let me read in smaller chunks than books and I get to see a lot more slang and dialect words, as well as different peoples' perspectives in general. Naver has a [webtoon app](https://itunes.apple.com/us/app//id315795555?mt=8) which makes keeping up with them really easy! Most importantly, it's free and I don't need to physically own the book–which matters when you're living out of two 26L and 40L backpacks.

I'm also seriously thinking about starting up Glossika practice once more just to force myself to speak way, way more so I can get more familiarity with speaking.

## New Grammar Points
### ~ㄹ/을 때마다 
Pretty simple grammar point: `~ㄹ/을` + `~마다`. {{ cite(num="0") }}

* 이 영화를 볼 때마다 너무 재미있다

### ~ㄹ/을 무렵
Advanced grammar point (so thus, relatively useless for me right now). "Roughly when / around when X". Similar to `~때쯤`.

* 해 질 무렵 출발할 것이다

### 한자 명사 + 시
Another slightly advanced grammar commonly found in advertisements. Formal.

* 모든 세트 구매 시 몬스터볼 보조 배터리

## Topics
### 기분
기분 is derived from 한자 while 느낌 is pure Korean, derived from 느끼다. 기분 refers to feelings, and typically more like "good/bad". 느낌 can refer to feelings as well as sensations and has a broader range in usage. {{ cite(num="1") }}

* 기분이 좋다 <- [I'm feeling] good/happy.
* 느낌이 좋다 <- [This feels] good/nice.
* 기분이 안 좋다 <- [I'm feeling] bad.
* 느낌이 안 좋다 <- [I have a bad feeling] about this / [This feels] bad.
* 지금 그런 기분 아니다
* 지금 농담할 기분 아니다
* 기분 내키는 대로
* 기분파

### 느낌
* 아무 느낌이 없다
* 느낌이 어땠다
* 뭔가 잘못된 느낌이다
* 누가 나를 보고 있는 것 같은 느낌이 들었었다

### 어쩌다
The plain form can be used as an adverb in the beginning of `~게 되다` sentences. It's similar to `만약` in that sense. {{ cite(num="2") }}

* 어쩌다 시험을 합격하게 되었다
* 어쩌다 그녀를 버스에서 보게 되었다

### 어쩌면
It's common in sentences that are unsure or guessing, i.e. `~ㄹ/을지(도)` or `~ㄹ 수도 있다` sentences. {{ cite(num="3") }}

* 어쩌면 그녀가 돌지도 모르다.
* 어쩌면 우리가 내일 갈 수 있을지도 모르다.
* 어쩌면 그녀의 남자친구가 그녀를 위해 선물을 모르다.

### 아무리
"Regardless/even if". Can be put at the beginning of a sentence, just like `만약` or `어쩌다`. {{ cite(num="4") }}
* 네가 나를 아무리 사랑해도 우리는 헤어져야 되다
* 아무리 그 제품을 만값으로 줘도 나는 안 살 것이다

## Phrases
### 구매 시
With each/every purchase

### 어쩔 수 없다
There's nothing that can be done {{ cite(num="5") }}

### 느낌이 있다
[It] has a feel/vibe/atmosphere. Can be used with a restaurant, place, etc.

## New Vocabulary
These are words that I didn't know while conversing 1:1 at my language exchange.

| Word | Meaning |
| :---: | :---: |
| 거실 | living room |
| 경찰서 | police station |
| 관심 받다 | to receive attention(interest) |
| 금지 | ban, to prohibit |
| 김 | steam |
| 김이 서리다 | to fog up, mist over |
| 단계 | level, stage, step |
| 더럽다 | to be dirty, fithy |
| 돌아다니다 | to wander around, roam | 
| 만족(하다) | satisfaction/contentment, to be satisfied | 
| 버튼을 누르다 | to push, press, supress |
| 변하다 | to change, vary |
| 보수적 | conservative |
| 비교(하다) | comparison, to compare |
| 성우 | voice/dubbing actor |
| 연기(하다) | performance, to perform/act |
| 우아하다 | to be elegant, graceful |
| 움직이다 (몸을) | to move (physically) |
| 점수 | score, mark, grade |
| 젖다 | to get wet |
| 주목 받다 | to receive attention |
| 지저분하다 | to be dirty, messy, unclean |
| 직장인 | office worker |
| 짖다 | to bark |
| 최대 | the most, maximum |
| 최소 | the smallest, minimum |
| 추상걱 | abstract |
| 털 | fur, hair |
| 하숙 | room and board, lodge |
| 형식 | form, convention |
| 환경 | environment, circumstances |
| 활발하다 | to be lively, animated |
