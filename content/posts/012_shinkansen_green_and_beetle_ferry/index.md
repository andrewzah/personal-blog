+++
title = "Taking the Japanese Shinkansen and Beetle Ferry"
slug = "japanese-shinkansen-beetle-ferry"
date = 2019-03-30
draft = true

[taxonomies]
tags = ["japan", "korea", "trains", "ferries"]
categories = ["travel"]

[extra]
keywords = "japan korea trains ferries travel shinkansen beetle ferry experience"
summary = "I ride the Shinkansen and Beetle Ferry, then write about it."
+++

Earlier this month, I had the opportunity to travel to Japan from South Korea. I flew from Incheon Airport to Narita Airport, and returned from Fukuoka, Japan to Busan, South Korea.

## Shinkansen
### Price
Before we begin, let's discuss the elephant in the room: the price. Prior to arriving in Japan I had done just about zero research, so I was quite surprised when I saw green car tickets from Shin-Osaka–Hiroshima going for about $220. Comparatively, tickets from Seoul to Busan via KTX are about $58. However as I've done more research I've realized that trains around the world can just be.. expensive sometimes.

[This answer](https://www.quora.com/Why-is-Japans-Shinkansen-so-expensive) on Quora sheds some light as to why Shinkansen tickets are so expensive. However, it's significantly cheaper for foreign travelers since they can get the JR pass.

#### JR Pass for Foreign Travelers

Foreigner travelelrs *only* can buy a [7, 14, or 21](http://www.japanrailpass.net/en/about_jrp.html#bm_002) day pass. As of today, 3/30/2019, you may no longer be able to even buy these passes within Japan. Apparently from 2016–2019 an experiment was conducted, allowing foreigners to purchase passes at most Shinkansen station sales offices.

Currently a 7-day pass for an adult is about $260 USD for a regular seat, or $350 USD for a green car seat.

There are two cons with this pass. First, you have to reserve seats in person at a Shinkansen office, which might mean waiting in line if you go at a busy time, especially at a busy station like Shin-Osaka. You can ride without reservation in the unreserved cars (usually there are 3), but you might have to stand.

Second, you can NOT ride the Nozomi (のぞみ) or Mizuho (みずほ) trains. These run more often than the Sakura (さくら), Hikari (ひかり), or Kodama (こだま) trains.

On the upside, the JR pass gives free travel on *any* JR train line, including local trains in Tokyo, etc. Instead of buying train tickets you can just show the pass to the staff at the turnstiles.

#### Green vs Ordinary

[This article](https://www.insidekyoto.com/jr-trains-green-cars-versus-ordinary-cars) breaks down the differences, replete with pictures. Green cars have less seats and are quieter.

I chose to go with the green car because I worked while riding the Shinkansen. Less seats means less people to make noise–and because they're more expensive–hopefully less foreigners that don't respect noise levels.

Note that the Kodama (こだま) train doesn't have green cars.

### Tips

For the Tokaido–Sanyo line, use the smartEx app to look at train schedules. For other lines, they should also have apps. This way you can figure out a plan and write the connections down so reserving the tickes at the Shinkansen office goes faster.

### Verdict
I think the pass is totally worth it. I personally went with the Green car pass, but the regular seats are still nice. If I weren't working while taking the Shinkansen I would probably get the ordinary pass.

Outside of the pass, it depends on whether you want to take a plane (plus local trains to each airport) or an overnight bus. An ordinary seat on a Nozomi train every once in a while is great.

## Beetle Ferry
