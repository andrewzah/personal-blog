+++
title = "Books for 2018"
slug = "books-for-2018"
date = 2018-02-24

[taxonomies]
tags = ["books", "philosophy", "programming"]
categories = ["life"]

[extra]
keywords = ""
summary = "Books that I want to read for 2018"
+++

My biggest regret of 2017 was reading few books. I want to change that for 2018, so here's a list of books that I am reading, or want to read—and why.

<!-- more -->

## Korea's Place in the Sun

Despite the title (which has nothing to do with Japan), this book is an elegant summary of East Asian history pertaining to Korea.

Having lived in Korea, I want to learn more about its history. Did you know that Korea invented a printing press about 200 years before Gutenberg?

## Programming Rust

I want to learn Rust well. This book has a lot of recommendations and, from what I've read so far, is compelling.

## Peopleware: Productive Projects and Teams

We often read books on programming, but businesses are run by humans. In addition to programming, I want to improve my interpersonal / "soft" skills.

So far this book has been a page turner. It's suprisingly short but gives a lot of material to think about, in a humorous fashion.

It does *not* reflect well on managers and executives. Bad ones, that is.

## The Timeless Way of Building

Written by Christopher Alexander, *Peopleware* references this book a few times. The premise is compelling and I don't want to only read programming-oriented books.

## The Road to Wigan Pier (!)

I heard about this book when listening to one of Jordan Peterson's interviews. George Orwell's critique so far is profound.

> I have known numbers of bourgeois Socialists, I have listened by the hour to their tirades against their own class, and yet never, not even once, have I met one who had **picked up proletarian table-manners**.

> The fact that has got to be faced is that to abolish class-distinctions means abolishing a part of yourself. Here am I, a typical member of the middle class. It is easy for me to say that I want to get rid of class-distinctions, but nearly everything I think and do is a result of class-distinctions. All my notions — notions of good and evil, of pleasant and unpleasant, of funny and serious, of ugly and beautiful — are essentially middle-class notions; my taste in books and food and clothes, my sense of honour, my table manners, my turns of speech, my accent, even the characteristic movements of my body, are the products of a special kind of upbringing and a special niche about half-way up the social hierarchy.

(emphasis mine)

I may not write a book review for all of these books, but I definitely will for this one.

## The Open Society and Its Enemies

I find it difficult to not purchase philosophy texts. This book piqued my interest due to Karl R. Popper's quote on the [Paradox of Intolerance][paradox].

> Unlimited tolerance must lead to the disappearance of tolerance. If we extend unlimited tolerance even to those who are intolerant, if we are not prepared to defend a tolerant society against the onslaught of the intolerant, then the tolerant will be destroyed, and tolerance with them. 

> In this formulation, I do not imply, for instance, that we should always suppress the utterance of intolerant philosophies; as long as we can counter them by rational argument and keep them in check by public opinion, suppression would certainly be unwise. But we should claim the right to suppress them if necessary even by force; for it may easily turn out that they are not prepared to meet us on the level of rational argument, but begin by denouncing all argument; they may forbid their followers to listen to rational argument, because it is deceptive, and teach them to answer arguments by the use of their fists or pistols. We should therefore claim, in the name of tolerance, the right not to tolerate the intolerant.

I'm also interested in Popper's perspective as he was born in 1902 in Austria, and was directly affected by the Nazi annexation.

[paradox]: https://en.wikipedia.org/wiki/Paradox_of_tolerance 

## 아마존 웨이 — Amazon Way

I saw this at 부산역 — Busan Train Station — and decided I'd try to read an entire Korean book in 2018. Not my brightest idea, but I'll slowly do it.

This page will be updated as I continue to finish books and find new ones.
Of course, recommendations are always welcome.
