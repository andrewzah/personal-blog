+++
title = "Making a Discord bot in Crystal: Part 0"
slug = "making-a-discord-bot-in-crystal-part-0"
date = 2018-03-05

[taxonomies]
tags = ["discord", "crystal"]
categories = ["programming"]

[extra]
keywords = "tutorial lesson programming crystal discord bot json"
summary = "This walks through building a ping/pong bot in Crystal."
+++

Today, let's start with a simple goal: Get a bot up and running via a JSON configuration file, in Crystal, that responds to a "!ping" command.

<!-- more -->

## Crystal and Shards installation

In order to install Crystal, you can follow the [docs][crystal_docs] for your operating system.

You can verify Crystal and Shards are installed by checking their versions:

```bash
λ shards --version
Shards 0.7.2 (2018-01-27)
```
```bash
λ crystal -v
Crystal 0.24.1 (2018-01-27)

LLVM: 5.0.1
Default target: x86_64-apple-macosx
```

[crystal_docs]: https://crystal-lang.org/docs/installation/

## Creating the project repo 

Crystal provides an `init TYPE NAME [DIR]` command for us generate a basic file structure. You can pass `app` or `lib` for the type, and you can optionally have a different directory name than the project name.

For now, let's go ahead and do `crystal init app mybot`. The generated structure looks like this:

```cr
├── LICENSE
├── README.md
├── shard.yml
├── spec
│   ├── mybot_spec.cr
│   └── spec_helper.cr
└── src
    ├── mybot
    │   └── version.cr
    └── mybot.cr
3 directories, 7 files
```

`LICENSE` (MIT by default) and `README.md` are self explanatory. `spec` stores our tests, which can be run with `crystal spec`.

`shard.yml` is the YAML configuration file for shards. Running this will generate a `shards.lock` with a snapshot of the dependencies.

`src` is where our code lives. By default, `mybot.cr` will include files in `src/mybot`, and `version.cr` is used to track our project using [Semantic Versioning][semver], starting at `0.1.0`.

[semver]: https://semver.org/

## Creating your bot

In order to run `Discord::Client`, we need a bot token from [Discord's apps section][devapps]. You'll need to create a Bot if you haven't already. **Note:** Make sure to click "Create a Bot User", as there's a difference. Next, click "show token" and make sure to save that value for a moment. Copy the Client ID as well.

[devapps]: https://discordapp.com/developers/applications/me

Now that the bot has been made, we have to add it to our server.
On your bot's application page, click `Generate OAuth2 url`. This will open a dialogue showing all of Discord's options for a server.

For now, our bot only requires `Send Messages` and `Read Message History`, *but* I imagine you'll want it to things later down the line such as editing channels, removing content, etc. You can request those things here, or do it manually on your Discord server by giving the bot a moderator `role`, etc.

## Reading and Writing JSON

Let's create a `config.cr` file to manage our configuration file.
```cr
# src/mybot/config.cr

module MyBot
  class Config
    JSON.mapping(
      token: String,
      client_id: UInt64
    )
  end
end
```

Let's break that down. First, we create a `Config` class, and then [JSON.mapping][api_json_mapping] handily lets us describe how our class gets mapped to and from JSON. We must define the type, and token is a `String`. `client_id` is an unsigned 64bit integer.

[api_json_mapping]: https://crystal-lang.org/api/0.23.1/JSON.html#mapping-macro

So now we need to create the file! Let's put it under `conf/bot.json`.
```json
{
  "token": "Bot xxxxxxxxx",
  "client_id": yyyyyyyyyyyy
}
```
Note: We have to prefix our token with "Bot ".

Now we don't want to share that token with the world… Make sure to modify your `.gitignore` file:
```bash
λ echo "/conf" >> .gitignore
```

Almost there. Now in `src/mybot.cr`, we can load up our dependencies and config file.
```cr
# src/mybot.cr
require "json"
require "./mybot/*" # Crystal supports wildcard matching

module MyBot
  file = File.read(File.join("conf", "bot.json"))
  config = Config.from_json(file) 
  puts config.token
end
```

First, we need to load `json` from the standard library, and our new config file. `File.join` lets us join files and directories in a system-agnostic manner ("\\" versus "/"). Once we read the file, we can use `Config.from_json`, which is provided automatically when we use `JSON.mapping`.

If everything was done correctly, the output of `crystal run src/mybot.cr` should be your token!

## Starting up Discord::Client

The [discordcr][discordcr] library wraps Discord's API for us and provides basic functionality. To install it, add this to your `shard.yml` file under `dependencies`:

[discordcr]: https://github.com/meew0/discordcr/

```yaml
dependencies:
  discordcr:
    github: meew0/discordcr
```

Then run `shards install`. This will grab the library for us and create a `shard.lock` file.

Now we can edit `src/mybot.cr` again:

```cr
require "json"
require "discordcr"
require "./mybot/config"

module MyBot
  file = File.read(File.join("conf", "bot.json"))
  config = Config.from_json(file) 

  client = Discord::Client.new(
    token: config.token,
    client_id: config.client_id
  )
end
```

If everything was done correctly, `crystal run src/mybot.cr` should produce nothing.

## Listening to input

`Discord::Client` comes with [many instance methods][discord_client]. We're going to be using `#on_message_create` which gives us a payload of type [Discord::Message][discord_msg]. 

[discord_client]: https://meew0.github.io/discordcr/doc/master/Discord/Client.html
[discord_msg]: https://meew0.github.io/discordcr/doc/master/Discord/Message.html

```cr
# src/mybot.cr
# ...
client.on_message_create do |payload|
  if payload.content.starts_with? "!ping"
    client.create_message(payload.channel_id, "Pong!")
  end
end

client.run
```

`#on_message_create` takes a block which lets us do what we want. If the payload starts with `!ping`, we create a message in the same channel as the payload with the text `Pong!`.

`client.run`, well, runs the bot. This will block so anything after this point will not run. We'll explore asynchronous execution through fibers later.

Let's check to see if our bot works.

{{ image(url="https://i.imgur.com/zbR6HrC.png", desc="Ping Test in Discord") }}

Hooray! Next time, we'll begin to explore a better system for commands. In the meantime, check out [Discord::Client's documentation][discord_client] and explore to see what you can do!
