+++
title = "Korean Progress #1"
slug = "korean-progress-1"
date = 2019-02-24

[taxonomies]
tags = ["korean", "learning"]
categories = ["korean-progress"]

[extra]
keywords = "korean hangeul language learning methods self study progress"
summary = "My Korean learning progress #1"
citations = [
  ["~아/어/여 놓다", "https://www.howtostudykorean.com/unit0/unit0lesson1/"],
  ["Almost: 거의 and ~ㄹ/을 뻔 했다", "https://www.howtostudykorean.com/unit-3-intermediate-korean-grammar/unit-3-lessons-59-66/lesson-66/"],
  ["~아/어/여 버리다", "https://talktomeinkorean.com/lessons/l9l2"],
  ["~ㄴ/는다기보다는", "http://su-eop.tumblr.com/post/158779767016/adv-ii-lesson-1-%EB%8A%94%EB%8B%A4%EA%B8%B0%EB%B3%B4%EB%8B%A4%EB%8A%94-rather-than-say"],
  ["I *did* do V, but…: 기는 하다", "http://www.mykoreanramblings.com/2014/09/21/i-did-do-v-but-%EA%B8%B0%EB%8A%94-%ED%95%98%EB%8B%A4/"],
  ["~ㄹ/을 테니까", "https://www.howtostudykorean.com/upper-intermediate-korean-grammar/unit-4-lessons-92-100/lesson-100/"],
  ["GoBilly ~ㄹ/을 테니까", "https://www.youtube.com/watch?v=m0-D6fsGui8"],
  ["~ㄹ/을 텐데(요)", "https://www.howtostudykorean.com/upper-intermediate-korean-grammar/unit-4-lessons-92-100/lesson-100/"],
  ["GoBilly ~ㄹ/을 텐데(요)", "https://www.youtube.com/watch?v=m0-D6fsGui8"],
  ["~조차", "http://www.koreantopik.com/2017/05/l2g38-n-grammar-even-not-only-others.html"],
  ["Depending on (~ㅁ/음에 따라서)", "https://www.howtostudykorean.com/unit-5/lessons-101-108/lesson/"],
  ["Depending on (~느냐에 따라서)", "https://www.howtostudykorean.com/unit-5/lessons-101-108/lesson/"],
  ["Depending on (~따라서)", "https://www.howtostudykorean.com/unit-5/lessons-101-108/lesson/"],
]


+++

While studying Korean, sometimes I get a bit lazy or lose some motivation. I decided to start this weekly series as a way to track what I've been learning and to motivate myself to keep studying.

<!-- more -->

This first one will be a little bigger as I have a bunch of stuff that I've studied recently without dates.

## Things I'm struggling with

Right now the biggest issue for me is comparing and contrasting points. And as always, speaking naturally (not textbook-like) is still tough.

## Current learning methods
* Studying grammar points daily.
* Going to a language exchange where I get 1-on-1 for an hour. Right now I go about 2-3 times per week.
* Using [Anki](https://apps.ankiweb.net/) to memorize and retain words.
* Texing friends in Korean.
* Surprisingly, playing [Donut County](https://itunes.apple.com/us/app/donut-county/id1292099839?mt=8<Paste>) on iOS. It's so unexpected to see a game that actually has a Korean translation!

Forcing myself to practice with Anki has become rough once again. I set my review limit to 125 and new daily words limit to 20, now 18. So it takes me about 50-60 minutes if I don't miss a day, or about 80-90 minutes if I do. If I make myself commute 40-50 minutes I've found that I can knock it out reliably, but on the weekend I have zero motivation. Memorizing words just... sucks, even if I use my own deck instead of someone else's premade deck (which I do).

Donut County is pretty cool since I get to see some real Korean conversations among friends, but in bite-sized pieces so I don't get overwhelmed. My goal right now is to understand as much as I can without looking anything up, and then I'll play the game once more whilst studying each sentence.

Going to the language exchange has been working wonders but it's been diminishing returns. It's slowly making me more and more comfortable with speaking in Korean but I really have to sit down aftwards and analyze the mistakes I made. Otherwise they tend to come up again. Nevertheless going to this exchange at least 2 times a week has improved my Korean skills *immensely*.

## Grammar Points
### ~아/어/야 놓다
This indicates an action has been done and is still in that state. It's possible to translate it with the perfect tenses, but it doesn't map neatly to English. {{ cite(num=0) }}

* 문을 열**어 놓다**.
* 집을 정리하려고 해서 이불을 **개 놓아** 주세요.
* 엄마가 계란 볶음밥을 만들**어 놓았다**.

### ~ㄹ 뻔 하다
Used when one almost started an action–i.e. "I almost fell". The action did not start, but almost did. {{ cite(num=1) }}

* 재가 거의 넘어질 뻔 했다.
* 우리 사이가 어근나서 싸울 뻔 했다.
* 컵에 물을 가득히 따르다가\* 물이 넘칠 뻔 했다.  
Note that 따르다 here means to overflow, not to follow.

### ~다거나
~다고 하다 + ~거나. Really simple form. One could translate it as "(phrase) or something..".

* 힘들다거나 하면 저에게 얘기하세요.
* 바쁘다거나 피곤하다고 해서 일을 자꾸 미루면 안 되다.
* 소리가 컸다거나 하면 앞으로 조심하겠다.

### ~아/어/여 버리다
Expresses that something has completely finished, and/or someone is relieved or upset/surprised at the result. {{ cite(num=2) }}

* 오늘도 늦게 일어나 버렸다.
* 제가 달리는 동안 열쇠를 잃어 버렸다.
* 드디어 검퓨터를 팔아 버렸다.
* 지루한 대통령의 연설을 끝나 버렸다.

### ~(/ㄴ/는)다기보다는, (이)라기보다는
"It's not that (x) but rather (y)". `다는` here often gets contracted to `단`. {{ cite(num=3) }}

* 내가 한국인 사귀하고 싶다기보다는 그냥 한국에서 살아서 ㅇㅇ.
* 촌스러웠다기보다는 그 옷에는 안 어울리는 것 같았다.
* 그를 좋아한다기보다는 존경한다는 표현이 맞을 것이다.

### ~기는 하다
동사 + "but (x)". Sometimes it could be translated as "even though (x), (y)..". Typically gets used with ~는데 or ~지만. For 형용사s, it's ~한데. For 동사s, it's ~하는데. {{ cite(num=4) }}. `기는` here often gets abbreviated to `긴`.

* 나는 소설 책을 읽기는 했지만 기억이 안 나다.
* 나는 영국사람이기는 하지만 생선과 감자튀김을 전혀 안 먹다.

### 빨갛다 and 뻘겋다
The vowels in some words can be changed to their mirroring vowels, in order to intensify the original verb/adjective. This is prevalent with color words.

* 노랗다, 누렇다
* 파랗다, 퍼렇다
* 까맣다, 꺼멓다

### ~ㄹ/을 테니까
~테 + ~니까. Used to make an assumption about something to justify or explain some clause/reasoning. {{ cite(num=5) }} {{ cite(num=6) }}

* 많이 추**울 테니까** 조심하세요
* 내일은 공부해야 **할 테니까** 오늘 만날까요?
* 김밥 맛이 비슷**할 테니까** 굳이 그 식당 까지 안 가도 되다

### ~ㄹ/을 텐데
Like ~테니까 but with ~는데. In certain sentences, it's awkward to use with yourself as the subject, since sometimes you know something and can't assume. Example: 나는 미국 사람일 텐데– totally weird to use with yourself as the subject, since you would know if you are American or not, typically. ~인데 would work there instead unless you were making an assumption about someone else. {{ cite(num=7) }} {{ cite(num=8) }}

* 많이 추울 텐데 괜찮겠다?
* 내가 했었을 텐데...
* 키가 컸으면 그 공을 잡을 수 있었을 텐데.
* 시간이 있었다면 갔을 텐데.

### ~조차
"Not even (n)", "not so much as (n"). Used in negative sentences. {{ cite(num=9) }}

* 그 남자의 이름조차 몰라요
* 친구가 시험 준비할 생각조차 못 하는데 어떻게 합격하나요?
* 지각해서 샤워할 시간조차 없어요

### ~고 싶어하다
Same as ~고 싶다 but used when referring to other peoples' desires.

### ~ㅁ/음에 따라(서)
Similar to ~을 때, but means more like "as I (do something)". It stresses that there was some sort of process involved. {{ cite(num=10) }}

* 제가 유럽에 여행함에 따라서 유럽문화에 관깁이 생겼어요.

### ~느냐에 따라(서)
Used with a verb stem, this means "depending on (where/why/how)..." or "depending on if...". {{ cite(num=11) }}

* 우리가 내일 가느냐 안 가느냐에 따라서
* 어떻게 공부하느냐에 따라 미래가 달라질 수 있다

### ~에 따라(서)
따르다 means to follow. ~에 따라서 means "following (x)", or more idiomatically, "according to"/"in accordance with ...". {{ cite(num=12) }}

* 사람들은 그 나라의 문화에 따라 행동을 해 야 돼요
* 길 안내에 따라서 우리는 저 쪽으로 가 야 돼요

## Phrases
### 저라면 / 저였으면
Literally "If it were me", "If it was me". This one is really simple but useful to know.

### 그렇고 .. 기도 해도
"well, that and ..."

### 그렇기도 한데
"well, that but ..."

## New Vocabulary
These are words that I didn't know while conversing 1:1 at my language exchange.

| Word | Meaning |
| :---: | :---: |
| 긍정적 | positive, affirmative |
| 기름지다 | to be fatty, greasy, rich |
| 기복이 있다 | to have ups and downs |
| 나누다 | to divide/split up (sth) |
| 나뉘다/나누이다 | to be split / divided |
| 단점 | disadvantage, weakness |
| 두르다 | to put up / wrap (sth) *around* sth |
| 둘러보다 | to look around, browse |
| 미세먼지 | fine dust |
| 부정적 | negative, pessimistic |
| 수리(하다) | to accept, receive (sth) |
| 수리(하다) | to repair, fix something (formal) |
| 시차 | time(zone) difference |
| 신용(하다) | credit, credibility, to trust |
| 아깝다 | stronger than 아쉽다, to be a shame |
| 아쉽다 | for sth to be a bummer |
| 안타깝다 | to feel sorry (for smn/sth) |
| 어른 | adult, grownup, senior |
| 어린이 | child (under 12) |
| 옮기다 5 | to express sth via writing/words |
| 옳다 | proper, correct (formal of 맞다) |
| 유동적으로 | fluidly, flexibly |
| 유학 | study abroad |
| 장점 | advantage, strength |
| 죄를 짓다 | to commit a crime, sin |
| 중고 | used, secondhand item/article |
| 청소년 | teenager, youth |
| 황사 | Yellow/Asian dust |

## References
