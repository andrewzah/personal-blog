+++
title = "Finishing that Marathon"
slug = "finishing-that-marathon"
date = 2018-11-03

[taxonomies]
tags = ["learning", "motivation"]
categories = ["general"]

[extra]
keywords = "how to motive accomplish motivation finishing goals"
summary = "It's not that hard to shine brightly. But how many people continue to shine years later?"
show_summary = true
+++

17 months. I realized today that it has been 17 months since I first started learning Korean. 17 months ago, I flew to Korea and lived there for about 8 months. Then I came back to the US 9 months ago. I'd like to say my Korean ability reflects 17 months, but it does not. This has to do with `motivation`, or my occasional lack thereof.

## It's easy to start off strong
The beginning of anything, especially learning a language, is always the easiest part. You go from nothing to something. With languages, you go from not knowing anything to knowing a few things in that domain- an infinite% improvement!! And afterwards your ability will continue to skyrocket- relatively speaking.

For real languages this amounts to learning the alphabet or script, and some very basic words or phrases. Then more words and some beginner grammar. For programming languages, it's the code syntax and style, then the surrounding ecosystem, etcetera. You can keep this up for a while, months, even a year if you're lucky. There are many things to learn but they're still fresh, exciting, new.  

Along this journey, people start to fall off for various reasons, but there are two big ones. First, the law of diminishing returns. You go from infinite! increase, to 100% increase, to 10% increase, until it becomes more of a steady drip.  Second, in the beginning of any field, study, or profession, you cannot know how little you actually know. How little ability you actually have. Don't worry, you will realize this once you start to become more intermediate, after longer exposure—maybe 3-4 months, maybe a year. For languages, this is typically when it becomes frustrating. You're no longer impressed by "Hi, my name is Andrew"-style basic speech... You're yearning for you know, actual conversations. 

Yet you can't.

And now, you know that you *have* to study way more in order to reach that level. This is vexing—since we don't tend to look at things from a relative standpoint. As improvement comes day by day it's tough to realize just how much we've improved! You can't do advanced things yet *but that's okay*. That's why we separate intermediate and advanced, I suppose. So at this point, you either become dejected and eventually stop, or you have a new fire lit under you, pushing you forward even harder.

{{ image(url="https://s3.amazonaws.com/andrewzah.com/posts/005/lawofdimin.png", desc="Graphic demonstrating the Law of Diminishing Returns") }}

## Keepin' on is astoundingly difficult
Once you get past the hurdle of keeping on despite realizing how little you actually know, it just becomes similar to `work`. I say similar, because you're still *studying*. You're NOT just showing up- that's useless. But you ARE still doing critical analysis, reflecting, and growing.

You just have to wake up, every day, and keep at it. Endlessly. At this point you really need to have the fires of passion burning, to sustain this.

You'll still find enjoyment in the studying, certainly, why else would you be doing this? Yet it just isn't quite as exhilarating as the beginning. There aren't much more large improvements- just little things here and there that add up to you becoming a proper professional, a superior-level language speaker, and so on.

These little nuggets can be fascinating in their own right too. For example, I've been using `git` for about ~3.5 years now? And I *just* learned about `git commit -v`. It's a small thing but it makes writing `git commit` messages much easier. Find a way to derive enjoyment fom discovering little things like this.

## Staying motivated
Anyone can study something an insane number of hours. Let's see you keep it up for a week, a month, a year, 10 years. Here are some things that helped me retain my passion in studying Korean and programming:

* **Taking breaks occasionally**. If I only had one recommendation, this would be it. There's just something about taking a week or longer break to clear your mind. Every single time I have done this, it only made me want to study Korean or programming anymore- I had missed it! Yet the time off allowed my mind to relax and ponder, giving me a fresh perspective.
* Finding and creating a circle of friends who also study what you study. When you learn new things, you have a responsive group to share them with! When you have questions, you have people to turn to. Properly done, a circle of friends will keep you quite interested and motivated. This is why the *Super Smash Bros.* tournament scene continues to live. It's hard to quit something when everyone you know does it.
* Changing up the mediums you study. Do you usually watch foreign tv shows to study? Try reading a short book or a comic. This one is more for language learners.
* Exploring different cultural things. My interest in Korean basically doubled when I started watching Korean TV shows like 아는 형님 and 런닝맨. Depending on the show you can learn a lot about the country's culture and history.
* Reflecting on your progress from say, a year ago. Look at your notes if possible, etc. This goes with what I said above about really putting things in perspective.
* Attending a language exchange or going somewhere that forces you to use the language. Personally I find language exchanges slightly forced/awkward sometimes, but they can be good too. When I visit Korea again, I'll be attending 문화원s (cultural centers that offer classes on traditional Korean arts—in Korean only).
* Teaching people who are less skilled than yourself. *You don't have to be a master to be a teacher*. I personally believe teaching is one of the best ways of retaining information. Wait until you're mid-intermediate/advanced though, unless it's very basic teaching. The last thing you want is to teach someone incorrect things.

## Conclusion  
These are some of the lessons I learned over time. When I moved back to the US, my motivation was killed for a while because I didn't explore other avenues like tv shows. I did study, but maybe once a week. Which was useless. I began improving again when I properly sat down, figured out my goals, and began studying every. single. day.

All I can say is Keep on keepin' on. Be honest with yourself and make sure you really want it- then dedicate yourself. Figure out reasonable, attainable goals, not perfectionist ones. Learning anything well is *never* a sprint—it's a marathon. Eventually you will be amazing at it, but only if you practice correctly and analyze yourself. Just showing up is not enough!!<Paste>
