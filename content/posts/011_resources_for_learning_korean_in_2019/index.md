+++
title = "Ultimate List of Resources for Learning Korean in 2019"
slug = "ultimate-list-of-resources-for-learning-korean-in-2019"
date = 2019-03-04

[taxonomies]
tags = ["korean", "learning"]
categories = ["education"]

[extra]
keywords = "how to learn korean hangeul language learning resources self study"
summary = "A collection of tools I found useful for learning Korean."
show_summary = true
+++


<!-- more -->

In the language servers I frequent, I often see newcomers asking about how they should start learning Korean, or what websites/books/apps to use, etc. I got tired of repeating myself, so I made this collection for people to easily copy/paste.

The following resources are all either resources that I *personally* have used while studying Korean, or resources that I've heard very good things about from fellow learners.

**Note**: {{hll(c="purple",t="*E*")}} = Elementary, {{hll(c="green",t="*B*")}} = Beginner, {{hll(c="blue",t="*I*")}} = Intermediate, {{hll(c="red",t="*A*")}} = Advanced.

I hope this is helpful, and let me know if I happened to leave out any good resources!

---

### Too Long; Didn't Read

Just looking to get started and don't wanna read this whole page? Here are some quick starting tips!

If you need to learn Hangeul, look at the Elementary section [below](#elementary).

These folks have really good youtube channels and websites with PDFs for free:

* [GO! Billy Korean](https://gobillykorean.com/)
* [TalkToMeInKorean](https://talktomeinkorean.com/)

This site has very detailed grammar explanations:

* [HowToStudyKorean](https://www.howtostudykorean.com)

These apps are great for beginners / in general:

* 세종 문법 초급 – Beginner lessons and grammar with audio: [iOS](https://itunes.apple.com/us/app/%EC%84%B8%EC%A2%85%ED%95%9C%EA%B5%AD%EC%96%B4-%EB%AC%B8%EB%B2%95-%EC%B4%88%EA%B8%89/id1347807237?mt=8), [Android](https://play.google.com/store/apps/details?id=air.org.kingsejong.grammarbasic&hl=ko)
* Naver Dictionary: [iOS](https://itunes.apple.com/us/app/naver-korean-dictionary/id673085116?mt=8) – [Android](https://play.google.com/store/apps/details?id=com.nhn.android.naverdic&hl=en)

Best of luck! Continue reading for more detailed explanations about these resources.

---

### Elementary
If you know nothing or very little about Korean, the first thing you should do is learn how to read Hangeul! Don't even bother learning romanization, or unlearn it if you already have learned it.

The *single* most important thing to learn here is that **Korean letters do not map nicely to English letters**. If you use romanization, you will seriously hinder your progress due to incorrectly learning how to pronounce things. You may see guides that say ㄷ is d, etc. Whenever you see this, you should mentally replace it with "ㄷ is *similar* to d, but is just ㄷ, not d".

#### Learn Hangeul in 90 Minutes – Start to Finish
{{hll(c="purple",t="*E*")}}: https://www.youtube.com/watch?v=s5aobqyEaMQ

Go! Billy Korean in general produces very high quality content for learning Korean, and is listed below.

#### Korean Word Structure and Basic Letters
{{hll(c="purple",t="*E*")}}: https://www.howtostudykorean.com/unit0/unit0lesson1/

I prefer text-based conent so I personally started with this. However when you learn Hangeul, make sure to listen to *something* so that you learn the sounds.

#### How to Read and Write in Korean 
{{hll(c="purple",t="*E*")}}: https://vimeo.com/ondemand/hangeul

This is a video course from TalkToMeInKorean that breaks down Hangeul and the mouth shapes you should be making. I highly recommend this short video series.

#### Jenny's Korean
{{hll(c="purple",t="*E*")}}: https://www.youtube.com/watch?v=0ZhOeA0RD9o&feature=youtu.be

This video is really popular for Hangeul learners.

## Websites
#### GO! Billy Korean's Website
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}: https://gobillykorean.com/category/full-lessons/

The website for GBK is basically a wrapper for Billy's youtube channel. You can watch his videos and download the related PDFs for free. Here you can easily find his content as opposed to digging through all of the videos on his channel. See youtube channels below for a full review.

#### TalkToMeInKorean's Website
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}+: https://talktomeinkorean.com/curriculum

TTMiK is also a great learning resource, particularly for beginners. They provide much smaller, more digestible lessons, in a better order. It's also free (you just need to make an account). The lessons have a podcast-like style and each lesson has a concise PDF.

Overall I think TTMiK is a great resource and it pairs well with How to Study Korean when you need a more detailed explanation. Sometimes the banter on the podcasts is slightly awkward but I still like the presenters.

#### HowToStudyKorean
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: https://www.howtostudykorean.com/

HtSK is an excellent resource born out of someone's fastidious notes and research. When I started learning Korean I pretty much used only this. All of the lessons are in-depth, to the point of sometimes having almost *too* much information..

One downside is the arrangement of the lessons–to put it simply, the order is a bit wack. For example, "because" is so, so common in language, but `~어/아서` isn't discussed until lesson 37. ~(으)니까 is also common isn't discussed until lesson 81. The concept of `~적` is discussed in lesson 16 but as a beginner/intermediate you won't be using ~적 words really.

I recommend following it linearly at least until lesson 32, but supplement it with searching naturally for grammar as you continue to speak in Korean. Lessons 26-32 are really important because they teach the `~는 것` principle.

#### Glossika
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}: https://glossika.com/

Glossika aims to provide fluency by giving you speaking exercises via spaced repetition. It's very similar to Pimsleur, except more modern. They offer a good deal of languages including Korean (but not Romanian, sadly)... I have a few friends who highly praise it, but haven't tried it yet. I think this month I'll try it and see how it goes.

The drawback here is that it's pricey, $30/mo per month or $25/mo yearly.

#### /r/Korean
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: https://reddit.com/r/korean/

/r/Korean is a fairly high quality subreddit for asking questions about Korean. Occasionally people post guides or series, but it's somewhat sporadic. There's good content here, but it's more useful if you already use reddit.

The level of the questions typically skews from beginner to intermediate.

#### Study with Bee
{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: https://studywithbee.com/

Bees is a regular in the LLK discord server who has been studying Korean for quite a while. She breaks down grammar well and provides good examples. She also occasionally writes about other things, like book reviews and studying different langauges. I recommend checking her blog out!

#### Naver English Dictionary
{{hll(c="purple",t="*E*")}}+: https://endic.naver.com/?sLn=en

This is a very good, if a bit conservative dictionary sourced from Daum. I still use this all the time out of habit, but I should use the Korean Foundation Dictionary more.

One issue is that on the desktop website, searching for grammar points like ~마다, etc, either yields nothing, or it yields very basic information. Searching on the mobile app gives way more information. The desktop site also has daily words and conversation, but the mobile app has more detailed examples and explanations.

#### Korean Foundation Dictionary
{{hll(c="purple",t="*I*")}}+: https://krdict.korean.go.kr/mainAction

The definitions in this dictionary are better, but they're in Korean. I recommend using this dictionary once you can read Korean definitions.

#### Naver Comic
{{hll(c="red",t="*A*")}}+: https://comic.naver.com/index.nhn

Here you can read Korean webtoons and browse by category. The mobile app is a bit nicer to use though, in my opinion. See below.

#### Audio Clip
{{hll(c="red",t="*A*")}}+: https://audioclip.naver.com/

A Korean podcast website. There is a complimentary app, see below.

#### Lang8, HiNative, iTalki, etc
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: https://hinative.com/en-US, https://lang-8.com/

These are sites where people can ask questions and receive answers. You may see them when searching online about specific words or grammar points. They are generally okay, but I would take it with a grain of salt, because it really depends on who answers. Sometimes the questions and replies are only in Korean.

## Youtube Channels

I'd say probably most of my Korean learning through youtube has been through content from GO! Billy Korean and TalkToMeInKorean. If you use no other learning resources, I would recommend these two channels.

#### GO! Billy Korean
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}+: https://www.youtube.com/user/GoBillyKorean

Billy is probably the best Korean teacher that I have seen. He teaches well to all skill levels. The majority of his content is elementary to intermediate, however he occasionally dips in to advanced topics, especially in his weekly topic livestreams. Most of his videos are about Korean itself (i.e. grammar) but he also has some videos about culture, and some interviews with Koreans.

Because Billy is a foreigner who learned Korean, I think he has a really good perspective for teaching, moreso than TTMiK. If you watch his videos (particularly the older ones) you can see that he's just extremely passionate about Korean, and teaching it.

#### TalkToMeInKorean
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}+: https://www.youtube.com/user/talktomeinkorean
Billy is probably the best Korean teacher that I have seen. He teaches well to all skill levels, but the majority of his content is elementary to intermediate.

#### SpongeMind
{{hll(c="blue",t="*I*")}}{{hlr(c="red",t="*A*")}}+: https://www.youtube.com/channel/UCrOP1_DfcuWWPuCQpGsVD-w

Spongemind sporadically uploads short 1-2 minute "inspiration videos". You can watch these to learn some new vocabulary words, and then repeat 10-20 times to practice saying the sentences. They also upload longer conversations / interviews with English and Korean subtitles.

## Apps
#### 세종 문법 초급
{{ image(url="https://i.imgur.com/Am2SUxw.jpg",desc="Sejong Beginner Gramar app interface pictures.") }}

{{hll(c="green",t="*B*")}}+: Apple/Android App Store

Sejong Beginner Grammar is a fantastic (and adorable) app that is geared towards beginners. You make a little avatar and get guided through beginner Korean conversations. There are grammar explanations and short drills afterwards. Once you know how to read Hangeul I recommend using this (and listening along, of course). Try to listen without looking first and see how much you can understand!

#### LingoDeer
{{hll(c="green",t="*B*")}}+: Apple/Android App Store

LingoDeer has guided lessons tailored to learning Asian languages. I have not used this personally, but many beginners seem to like it. LingoDeer is miles ahead of DuoLingo (which should absolutely not be used for Japanese or Korean, yet). The lessons also have audio, which is important.

The only issue I've seen here is due to the UI, it doesn't teach proper spacing sometimes, but that's a relatively minor issue.

#### Anki Flashcards
{{hll(c="purple",t="*E*")}}+: https://apps.ankiweb.net/docs/AnkiMobile.html

Note that there is a competitor who has a similar name (AnkiApp). Use the above link for the correct Anki.

I cannot praise Anki highly enough. It's a spaced repetition flash card app, so as you master a word, it shows up less and less frequently. You can (and should) make custom decks but beginners can use [Evita's premade deck](https://ankiweb.net/shared/info/4066961604) with 5k words. The author set words to appear in a specific order based on their usage. I recommend using this for 4-6 months, and then as you start consuming Korean content like webtoons/tv shows/etc, I suggest moving to your own deck with words that you saw.

Also, I *strongly* recommend making a deck with a field for sample sentences. Decks like Evita's just show the word.. but not *how* it's used. For beginner words like `mom` this is adequate, but as you learn more advanced words it quickly grows frustrating.

My personal deck has fields for: word type (noun, adverb, etc), sample sentences, hanja, and notes. Some people also use audio and images, but it already takes me long enough to sit down and add words without having to download those as well. I also use custom CSS styling to give those fields different colors.

One final warning: If you add 20 words a day, you will burn out eventually. I recommend limiting it to 10 new words per day, and 100 reviews per day, or less. Unless you can commit to doing Anki 45-60 minutes a day, every day, it will be very hard with the default settings. 

I use Anki so much that I'm planning on doing a writeup on it and its intricacies.

#### Naver English Dictionary
{{ image(url="https://i.imgur.com/SkYmTWr.jpg",desc="naver endic app interface pictures") }}

{{hll(c="green",t="*B*")}}+: Apple/Android App Store

This app is indispensable. When you study Korean you're gonna be looking words up a LOT, so having a good dictionary app helps. You can quickly switch between the Korean, Chinese, and Hanja dictionaries as well.

I can't vouch for the quality of the other Korean dictionary apps since I don't know what source they're using.

#### Papago
{{hll(c="purple",t="*E*")}}+: Apple/Android App Store

Translator app that's usually better than google translate. Do not rely on this. It can work with simple sentences but should only be a last resort. It almost always fails on non-basic sentences. They also added an honorific mode, but I've seen it fail. Rely on this at your own peril.

#### Naver Webtoon
{{hll(c="red",t="*A*")}}+: 네이버 웹툰 in the iOS/Android app stores

This app lets you easily browse webtoons from comic.naver.com. Here, you leave educational Korean territory and go into real Korean territory. These webtoons are written by and consumed by Koreans, after all. You'll see not only real Korean but slang and dialect as well, so it can be quite tough.

There may(?) be more simplistic webtoons aimed at foreigners, but I'm not aware of any so far. If you need a recommendation, 마음의 소리 (Sound of Your Heart) got turned into a netflix original series. Currently I'm reading [뚜벅뚜벅](https://comic.naver.com/bestChallenge/detail.nhn?titleId=709889&no=1&week=mon). If you use the app you can easily pick random ones.

#### Audio Clip
{{hll(c="red",t="*A*")}}+: 오디오 클립 in the iOS/Android app stores

A Korean podcast app. I have attempted to use this but I'm nowhere near advanced enough to listen to a full length podcast without getting frustrated.

You can filter by categories and download podcasts for offline listening.

#### HelloTalk
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: Apple/Android app stores

I hesitate to suggest this app. On one hand, you can talk with people via text or voice and receive corrections easily. On the other hand, I've heard of people kinda being creepy (despite HelloTalk explicitly prohibiting using it as a dating app).

I personally have used this app with no problems. You can post on your own wall and receive corrections, or you can message people directly.

## Community Servers
#### Let's Learn Korean!
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: https://discord.gg/e3H9Pde

LLK is a very active Discord server for learning about and practicing Korean! You can ask questions, post Korean sentences to be corrected, and otherwise chat with other learners about Korean and general topics! LLK has good moderation despite its size, so the conversations remain high quality in the learning channels.

I love this server because my questions are *always* answered promptly. The most I've had to wait is 7-9 hours or so. There are many advanced and native speakers.

#### GO! Billy Korean's Server
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: https://discordapp.com/invite/tJmt6zm

GBK also has a server, so if you watch a lot of his videos I recommend checking out this server. You can discuss in Korean and ask questions here too, but there are less people so it can take longer to get questions answered. Billy actually answers questions both here and in LLK.

#### /r/Korean's Server
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: http://discord.gg/rjjaNWN

The [/r/Korean](https://reddit.com/r/korean) subreddit also has its own discord server, but it too is small currently. If you use reddit and /r/Korean a lot, I recommend checking this one out.

Honestly, regular users from LLK (like me) are in all 3 servers. There are more discord servers for sure, but I can't personally vouch for them or their quality.

## Textbooks
### Korean Made Simple (Series)
{{hll(c="purple",t="*E*")}}{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: https://gobillykorean.com/korean-made-simple/

Not only does GO! Billy make videos, but he also recently has been publishing this learning book series! The books have vocabulary, grammar, examples, and practice sections with answer keys. I have not used them personally but I have heard good things from learners about it–I also just trust content coming from Billy.

An (optional) extra workbook is also available if you want even more practice.

### Korean Grammar in Use (Series)
{{hll(c="green",t="*B*")}}{{hll(c="blue",t="*I*")}}{{hll(c="red",t="*A*")}}+: https://www.amazon.com/gp/bookseries/B01M1S9ZK4

KGiU comes in Beginner, Intermediate, and Advanced levels. It very strictly focuses on grammar only, mapping it TOPIK levels. (Beginner: TOPIK 1-2, Intermediate: 3-4, Advanced: 5-6). Each grammar point has detailed explanations and breakdowns, sample sentences, and practice sentences with an answer key.

These books are fantastic resources for looking up and referencing grammar, but you can also use websites like HowToStudyKorean for free. In fact, some other websites are basically direct rips from these textbooks, images and all.

I personally just used HowToStudyKorean, and by the time I bought the beginner textbook I realized I already knew nearly all of the grammar... When I start studying for the TOPIK more seriously, I'll probably read the intermediate and advanced ones cover to cover.
