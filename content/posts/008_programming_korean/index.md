+++
title = "Korean for Programmers"
slug = "korean-for-programmers"
date = 2019-02-16

[taxonomies]
tags = ["korean", "programming", "linguistics"]
categories = ["general"]

[extra]
keywords = "korean programming language hangeul linguistics hangeul perspective"
summary = "Korean has similarities with functional programming languages."
show_summary = true
+++

I was inspired to write this after reading *German for Programmers*.{{cite(num=0)}} It's quite likely that I'm just biased to see things a certain way because I work with programming languages for a living, but I often find myself drawing comparisons among Korean and programming.

<!-- more -->

## Hangeul Basics
Before we can get into grammar, some explanation about Hangeul–the Korean alphabet–is needed. Korean is actually like English in that letters are put together, so you can read any Korean once you learn Hangeul. However, Korean letters join together in syllable blocks.

You may have seen the infamous "Learn how to read Hangeul in 10 minutes" graphic. While Hangeul is indeed very easy to learn, it does have exceptions–quite a few actually. So maybe you can read 독립 (independence), but the actual pronunciation is different from what's written due to ㄹ following ㄱ. These are called 받침{{cite(num=1)}} rules and one just has to memorize 'em.

### Grid System
Unlike Chinese or Japanese, Korean syllable blocks take the Hangeul letters and make them into grids in a particular order.

{{ image(url="https://i.imgur.com/Z3nHYsB.png", desc="Hangul Grid Order") }}

Each block contains a vowel and it's always the second character. The block are constructed depending on whether the vowel is vertical (ㅣ ㅏ ㅓ) or horizontal (ㅡㅜㅗ). If you want to say "ah", ㅇ is used as a silent character in the first slot–which makes 아: `ㅇ` + `ㅏ`.

Some vowels can also be combined: `ㅚ` is `ㅜ` + `ㅣ`, `ㅙ` is `ㅗ` + `ㅐ`.

For Korean fonts, all finished blocks (e.g. 쀍: ㅃㅜㅔㄹㄱ) have to be included because it's possible to construct them via grid rules, but a fair amount are just gibberish. Hangeul is represented by 5 unicode blocks in total and I recommend reading about how they get composed together.{{cite(num=2)}}

## Subject Elision, especially -you-

In English, we (almost) always need to specify the subject. I can't write just "Can't write." or "Ate an apple.". In Korean however, the subject isn't necessary in every sentence. In fact, always including it isn't natural at all! Korean is highly contextual, so if you say:

먹었어요 – literally: ate. --> interpreted as: "I ate"

In a lot of contexts, that would make most sense as (I) ate. However if someone asks you, "Did Charlie eat?", you can still say:

(네,) 먹었어요. – (yes,) ate. --> "Yes, Charlie ate."

Similarly if you ask someone else a question, it's usually obvious if they are the subject.

이 책을 읽었어요? – lit: this book read? --> "Did you read this book?"

It makes little sense to ask "Did I read this book?" in most situations. (If you want to muse to yourself like "ah, did I (already) read this book..?", that requires a specific grammar point.)

If you're wondering, people do clarify from time to time.

"먹었어요?" - ate? --> "(Did you) eat?"

"저요? / 저는요?" --> "(were you referring to) Me?" / "As for me?"

---

The subject/topic still has to be included when it changes or it's ambiguous. If you start off by saying "저는 ..." ("as for me, ..."), it's implied the following sentences will still be about you.

In this sense I *guess* one could say Korean leans toward being duck-ly typed–but that's a tenuous comparison. Anyway, it's different from English where the subject almost always must be specified (strongly typed...?), except in some spoken slang. Even very informal slang like "d'ya eat?" still includes the subject. We're playing a bit fast and loose here with the comparisons.

---

A word of caution: Using the direct words for "you" (너, 네가, 당신) is also quite rude unless you are close with someone. Papago and google translate often translate "you" to 너 or 네가. Don't use these until you know what you're doing.  당신 has different nuances and should also be avoided. It's really hard for English speakers to let go of "you", but you must.

## Sentences &amp; Grammar
### Sentence Order

English is an SVO (Subject Verb Object) language. "Billy ate an apple". Korean is an SOV language. "Billy apple ate". This reversal makes thinking in Korean actually quite difficult once one gets to causation and more difficult sentences.

English: I went to the park because it was sunny out today. Korean: It was sunny out today so I went to the park.

English: I was so tired I could barely walk. Korean: To the extent that I could barely walk I was so tired.

---

From the perspective of a native English speaker, I find this reversal utterly fascinating.  With English the last bit of the sentence usually has the important bit:

"Do you know where the *bathroom* is?"

You basically have to finish the whole sentence there before the other person can understand what you intend. Now with Korean:

*화장실*이 어디에 있는지 아세요?

I often can barely get this out in time before I start getting an answer.

### Finally, a sentence

We made it! Let's start with a simple sample sentence in Korean.

저는 사과를 어제 먹었어요.

<p>
  {{hlw(c="green",t="저(는)")}} {{hlw(c="red",t="사과(를)")}} {{hlw(c="pink", t="어제")}} {{hll(c="blue",t="먹")}}{{hlm(c="purple",t="었")}}{{hlr(c="yellow", t="어요")}}.
</p>

<p>
  {{hlw(c="green",t="subject + marker")}} {{hlw(c="red",t="object + marker")}} {{hlw(c="pink", t="adverb")}} {{hll(c="blue",t="verb stem")}}{{hlm(c="purple",t="past tense modifier")}}{{hlr(c="yellow", t="politness conjugation")}}.
</p>


There's already a lot going on here. The first difference you may notice is the markers. In English, one has to identify what the subject is. In Korean, topics and subjects are marked with 은/는 or 이/가, respectively. Objects get marked with 을/를. Just like the subject, these are not always necessary and get dropped in speech and texting all the time.

### Markers

Korean has a number of postpositional particles that imbue meaning, some of which vary if the last character is a vowel or not.
We can easily use a pure function to model this:

```rust
fn topic_marker(word: &str) -> &str {
  match word.last_character_type() {
    Vowel => "는",
    Consonant => "은"
  }
}

fn plural_marker() -> &str {
  "들"
}
```

Here are a few other markers (with simplified definitions). The text inside the parentheses is used if the last character is a consonant:

* ~에서 => from
* ~까지 => until
* ~(으)로 => several meanings. roughly-speaking it shows how/via what method or material something is carried out, or "toward" a place if used with "to go", etc.

### Pipelining Data Transformations

Where the previous sentence started to get interesting was at the end, with the verb, tense, and politeness. That's not all we can transform though.
The next two sentences here are "I closed the door" (informal impolite, then informal polite), after is "My parents closed the door" (formal, polite).

<p>
  {{hlw(c="green",t="나는")}} {{hlw(c="red",t="그것을")}} {{hll(c="blue",t="닫")}}{{hlm(c="purple",t="았")}}{{hlr(c="yellow",t="어")}}.
</p>

<p>
  {{hlw(c="green",t="저는")}} {{hlw(c="red",t="그것을")}} {{hll(c="blue",t="닫")}}{{hlm(c="purple",t="았")}}{{hlr(c="yellow",t="어요")}}.
</p>

<p>
  {{hlw(c="green",t="우리 부모님은")}} {{hlw(c="red",t="그것을")}} {{hll(c="blue",t="닫")}}{{hlm(c="orange",t="으시")}}{{hlm(c="purple",t="었")}}{{hlr(c="yellow",t="습니다")}}. (Note that I'm showing {{hll(c="orange",t="시")}}{{hlrw(c="purple",t="었")}} here separately to break down the components but they would get merged to 셨)
</p>

The main verb here is 닫다–to close. All Korean verbs end in 다, so the first thing we need to do is get the stem by removing `다`.

Now we need the honorific level. In the first two sentences I'm talking about myself, so I can't use honorifics. However when the actor is "my parents" it's common to use honorifics, which is `~(으)시`.

Before we can add a tense, we need to determine what vowel to add, and if it should merge or not. 닫 ends in a consonant, so merging is not possible, but the last vowel is 아 so we append `아`. (If the verb stem ends in a vowel, like `가`, the following vowel `아` would just get merged into `가`)

One way to do past tense is `~ㅆ`, which gets merged with the previous vowel. Other tenses can depend on the last character being a vowel or not, like future tense (`~ㄹ/을`).

Finally, we need to think about our relationship to the audience and append or merge a politeness/speech level. See politeness below for more details. 

We can model this as a basic pipeline à la Clojure:

```clojure
(defn conjugate-verb
  [subject verb speaker audience]
  (->> verb
    (remove-stem)
    (maybe-append-honorific subject)
    (append-or-merge-vowel)
    (append-or-merge-tense)
    (append-or-merge-politeness-level speaker audience)
  )
)
```

Note that there are even more transformations that we can apply depending on the grammar point and the nuance of what one is trying to say. Fun, isn't it? At least it's relatively consistent unlike English–even irregular verb rules are fairly regular.

### Adding to the Stack

> In linguistics, nominalization or nominalisation is the use of a word which is not a noun (e.g., a verb, an adjective or an adverb) as a noun, ...
> The term refers, for instance, to the process of producing a noun from another part of speech by adding a derivational affix (e.g., the noun legalization from the verb legalize).

In Korean, one can nominalize entire clauses and use them in other constructs! Korean lets you do this with the `~는 것` principle. 것 means thing, but any noun can be used in place of `것`. Based on the tense, verb type, and whether the verb ends in a vowel, `는` has variations like `~ㄴ`, and `은`. It can also combine with other grammar forms, like `던`, which is `더~ + ~ㄴ/은`. Digging into these would be beyond the scope of this post.

To give you a small example, one way to say "exit" in Korean is literally "going out place", or "a place that one goes out".

{{ image(url="https://i.imgur.com/D30wPwp.jpg", desc="A Korean subway exit sign.") }}

###### Source: Wikipedia 나가는 곳

<p>
  {{hlw(c="purple",t="나가다")}} means "to go out". {{hlw(c="red",t="곳")}} means place. A {{hll(c="purple",t="나가")}}는 {{hlw(c="red",t="곳")}} is a {{hlw(c="purple",t="going out")}} {{hlr(c="red",t="place")}}.
</p>

Note that 나가다 is a pure Korean word. To the right is 出口 (pronounced 출구), which also means exit, and is derived from classical Chinese. Just like English has Latin and Greek influences, Korean has Chinese and to a small extent, Japanese influences.

#### Sentence the First

So, let's take 'the girl walked to school'. In English and Korean this is straightfoward enough:

<p>
  {{hlw(c="green", t="여자는")}} {{hll(c="pink",t="학교")}}{{hlrw(c="purple",t="로")}}{{hlw(c="blue",t="걸어")}} {{hlw(c="blue",t="갔어요")}} — {{hlw(c="green",t="The girl")}} {{hlw(c="blue",t="walked")}}{{hlw(c="purple",t="to")}} {{hlrw(c="pink",t="school")}}
</p>

But what if you wanted to talk *about* that person? You could say "the girl *who* walked to school". In English, this these are known as relative causes. They can begin with `who`, `which`, `that`, `where`, etc, *following* the noun. Korean uses the `~는 것` nominalizer *before* the noun, which leads to:

<p>
  {{hll(c="pink", t="학교")}}{{hlrw(c="purple", t="로")}} {{hlw(c="blue",t="걸어")}} {{hlw(c="orange",t="간")}} {{hlrw(c="green",t="여자")}}!
</p>

Note that `갔` changed to `간`. `갔` is 가다 (to go) + `~ㅆ` (past tense). But the past tense nominalization form uses `~ㄴ (것)`. Instead of 것 (thing) we swapped it for another noun 여자 (woman).

Not that one would only say "the girl who walked to school" by itself, but we can now use the entire construct as a noun in other sentences: 

<p>
  {{hlw(c="green",t="저는")}} {{hlw(c="red",t="학교로 걸어 간 여자를")}} {{hlrw(c="blue",t="알았어요")}} — {{hlw(c="green",t="I")}} {{hlw(c="red",t="the girl who walked to school")}} {{hlrw(c="blue",t="knew")}}
</p>

#### Sentence the Second

We can try a more complex sentence now: "That's the place (that) I thought I went to!". First, we need to break it down in a sentence can that be nominalized. "I thought I went". Here we can use the grammar point `~ㄴ 줄 알다` which when used means the speaker thought something was true, but realized it wasn't–due to a lapse in judgement, etc.

<p>
  {{hlw(c="purple", t="제가")}} {{hlw(c="red",t="간 줄")}} {{hlrw(c="blue",t="알다")}} – {{hlw(c="green", t="I")}} {{hlw(c="blue",t="thought")}} {{hlw(c="red",t="(that)")}}{{hlw(c="purple",t="I")}} {{hlw(c="red",t="went")}}
</p>

You may have noticed that this grammar point itself uses `~ㄴ 것`, but with `줄` instead of 것! This 줄 is a bound noun, meaning that it can only be described by a `~는 것` clause. Outside of `~ㄴ 줄 알다`, 줄 can also be a regular noun meaning line/rope.

<p>
  {{hlw(c="green",t="그곳은")}} {{hll(c="red",t="제가 어디에 간 줄 알았")}}{{hlrw(c="yellow",t="던")}} {{hll(c="purple",t="곳")}}{{hlrw(c="blue",t="이야!")}} – {{hlw(c="green",t="That")}} {{hlw(c="blue",t="is")}} {{hlw(c="purple",t="the place")}} {{hlr(c="red",t="I thought I went to!")}}
</p>

#### Sentence the Third

Can we go even deeper?

<p>
  {{hlw(c="green",t="[나는]")}} ((({{hll(c="blue",t="상황이 억울하다고 말하는")}}{{hll(c="yellow",t="불평불만")}}){{hlrw(c="red",t="만 하는 사람은")}}) ({{hll(c="pink",t="별로 좋아하")}}{{hlr(c="purple",t="지 않는다")}}).
</p>

<p>
  {{hlw(c="green",t="[I'm]")}} ({{hlw(c="purple",t="not")}} {{hlr(c="pink",t="keen on")}}) ({{hlw(c="red",t="people who only")}} ({{hlw(c="yellow", t="complain")}}{{hlr(c="blue",t="that things are unfair")}})).
</p>

This sentence doesn't really translate 1:1 to English, as is the case with most intermediate/advanced Korean sentences.

---

Nominalizing with `~는 것` is my favorite aspect of Korean because it's an important grammar point that blew my mind once I learned how it worked. It's quite commonly used as well. In day to day usage I might say something like {{hlw(c="purple",t="the house (that)")}} {{hlw(c="green",t="I")}} {{hlw(c="red",t="used to")}} {{hlw(c="blue",t="live (in)")}} – {{hlw(c="green",t="제가")}} {{hll(c="blue",t="살")}}{{hlrw(c="red",t="았던")}} {{hlr(c="purple",t="집")}}, et cetera.

### Language Tidbits
These are some cool traits about Korean, or things related to this post, that don't necessarily have to deal with programming.

#### Politeness / Formality
The Korean language conjugates differently based on the status of speaker and intended audience. For example, one of the simplest ways to conjugate any verb is to add `~어/아/여` to it. This is based on the last *vowel*, not the last character. 

For example, you may have seen 감사합니다 before ("thank you", formal polite). This is 감사하다, merged with `~ㅂ니다` because `하` ends in a vowel. 고마워요 is another way to say thank you(informal polite): 고맙다 + apply `irregular ㅂ` consonant ending filter +  `~아/어/여요`.

```rust
fn get_vowel_for_verb(verb: &str, formality: Formality) -> {
  // ha = 하
  if verb.stem_ends_with_ha() {
    "여"
  } else {
    match verb.last_vowel() {
      "아" => "아"
      "오" => "아"
      "어" => "어"
      "우" => "어"
      "이" => "어"
      "의" => "어"
      "위" => "어"
    }
  }
}
```

Korean has seven speech levels {{cite(num=4)}}. When learning Korean, the `아/어/여요` and `~ㅂ/습니다` levels are commonly used, in that order. Using `아/어/여` (no `요`) to anyone other than close friends (who have agreed to use lowered speech) or young kids is rude. Foreigners get a pass at first but it's still impolite.

Plain (sometimes known as diary) form is also used, such as in diaries and books/novels.

English lacks this concept, as we use the same conjugation for everyone – "the prisoner ate", "the king ate", and "a God ate". What English does have is different registers{{cite(num=5)}}, such as when you text versus when you write an academic paper or a business email. This includes overly polite language like "Might you be interested in eating, sir?", but nevertheless the verb remains the same.

#### Quoting Statements
Quoting plain statements in Korean is very easy. All you need to do is take the sentence, conjugate the verb into plain form, and append `~고 (말)하다`. For verbs, `~ㄴ/는다` is the plain form. For adjectives, it's just `다`, or the base verb.

(저는) 먹었어요. {{hll(c="green",t="(제가) 먹었")}}{{hlrw(c="purple",t="다고 말했다")}} – I ate. I said I ate.

Depending on the type of statement, different particles than 다 are used.

* declarative => `다`
* inquisitive => `냐`
* propositive (let's ...) => `자`
* imperative or => `라`
* declarative with 이다 (to be) as the verb => `라`

이것을 좋아하다고? – (You said that) you like this?

Since this is used so much in speech, the (말)하다 (to say) part is often omitted. If you're learning Korean, expect to hear this a lot from natives because Korean pronunciation is tough.

#### Lack of Romanization

Why does this article lack romanization? Because romanization is bad. English and Korean sounds do not map neatly to one another.
The issue here is that Korean learners mentally map {some english sound} => {some hangeul letter} and it hurts their pronunciation skills immensely.
For example, you may see this in beginner resources:

d = ㄷ

Except this is wildly wrong because only ㄷ is ㄷ. It is *close* to d, in the same sense that fresh water is *close* to salt water. Close enough, right? If you're learning Korean, listen to videos that teach the sound, not the most approximte English letter.

I also have seen people write things like anyeonghasaeyo jal jinaeyo? or similar, and it hurts my brain and heart trying to read it.

Furthermore, romanization systems can change over time. 조 used to be romanized as Cho, now it's Jo. So when I read older books that have romanized Korean it forces me to go and learn the older system as well. 조 isn't Jo or Cho anyway... it's somewhere in between.

## Contributors

* Article – Andrew Zah
* Editing, sentence suggestions – 웁스

## References
* {{ footnote(num="0", url="https://wickedchicken.github.io/post/german-for-programmers/", title="German for Programmers") }}
* {{ footnote(num="1", url="https://www.howtostudykorean.com/unit0/unit0lesson1/", title="Grid Order ") }}
* {{ footnote(num="2", url="http://www.koreanwikiproject.com/wiki/%EB%B0%9B%EC%B9%A8", title="Final Consonant Exception Rules") }}
* {{ footnote(num="3", url="https://en.wikipedia.org/wiki/Korean_language_and_computers#Hangul_in_Unicode", title="Hangeul in Unicode") }}
* {{ footnote(num="4", url="https://en.wikipedia.org/wiki/Korean_speech_levels", title="Korean Speech Levels") }}
* {{ footnote(num="5", url="https://en.wikipedia.org/wiki/Register_(sociolinguistics)", title="English Registers") }}
